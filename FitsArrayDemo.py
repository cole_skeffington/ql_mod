# -*- coding: utf-8 -*-
"""
Created on Mon Jul 29 17:44:01 2019

@author: silve
"""

from astropy.io import fits
import numpy as np
import ql_mod as tbl
import matplotlib.pyplot as plt
import math
import matplotlib.ticker as ticker


# making some demo graphs


# ***** applying conditions to make data subsets

# ex) 1 -- all clouds_top at 10-16 LT --> column = 'clouds_top'
# -----------------------------------------------------------------        
column = 'cloud_gen'
filename = '3400-3700.fits'
o,t,lat,lon = tbl.trim_data(filename, column)
print 'o =',o
print 'lon =,',lon
x = t
mask = np.zeros(t.shape[0], dtype=bool) # empty array for boolean 'mask'
cond1 = 10 # LT lower bound
cond2 = 16 # LT upper bound
for i in np.arange(x.shape[0]): #iterate over trimmed LT array, populate mask bool array
    for j in np.arange(cond1, cond2 + 1):
        if j in x[i]:
            mask[i] = True
            break
        else:
			mask[i] = False
            
#print 'mask =',mask

filtered_o = o[mask] # creating filtered data from mask
filtered_lon = lon[mask]

#print 'filtered_lat =',filtered_o     #check the mask to see it worked

# actual graph for ex1) 
fig1, ax1 = plt.subplots()    
x = filtered_o
y = filtered_lon
for i in np.arange(x.shape[0]):
    for j in np.arange(y[i].shape[0]):
        ax1.plot(x[i],y[i][j],'ro')
plt.xlabel('Orbit Number',fontsize=12)
plt.ylabel('Longitude (degrees)',fontsize=12)
plt.title('General Clouds from 10-16 LT',fontsize=15)

#xint = range(min(x), max(x)+1)
#plt.xticks(xint)
ax1.set_xlim(min(x) - 1,max(x)+1)
ax1.xaxis.set_major_locator(ticker.MultipleLocator(2))
ax1.xaxis.set_minor_locator(ticker.MultipleLocator(1))

plt.show()
        
# ex) 2 -- bar of cloud brightness for different longitude ranges
# ---------------------------------------------------------------------

# actual graph for ex2)
    
n_ranges = 6
o1,lon1,s1,b1,g_c1 = tbl.from_val('7200-7600.fits','bright',1)
o2,lon2,s2,b2,g_c2 = tbl.from_val('7200-7600.fits','bright',2)
o3,lon3,s3,b3,g_c3 = tbl.from_val('7200-7600.fits','bright',3)
tot_detect = np.shape(b1)[0]+np.shape(b2)[0]+np.shape(b3)[0]
# brightness 1 over 6 longitude ranges
r11_b,r11_lon = tbl.in_lon_range(b1,lon1,0,60)
r21_b,r21_lon = tbl.in_lon_range(b1,lon1,60,120)
r31_b,r31_lon = tbl.in_lon_range(b1,lon1,120,180)
r41_b,r41_lon = tbl.in_lon_range(b1,lon1,180,240)
r51_b,r51_lon = tbl.in_lon_range(b1,lon1,240,300)
r61_b,r61_lon = tbl.in_lon_range(b1,lon1,300,359)
# brightness 2 over 6 longitude ranges
# notation: rxy is range x (of 4), brightness y (of 3) ; b is brightness array, lon is longitude range corresponding to it
r12_b,r12_lon = tbl.in_lon_range(b2,lon2,0,60)
r22_b,r22_lon = tbl.in_lon_range(b2,lon2,60,120)
r32_b,r32_lon = tbl.in_lon_range(b2,lon2,120,180)
r42_b,r42_lon = tbl.in_lon_range(b2,lon2,180,240)
r52_b,r52_lon = tbl.in_lon_range(b2,lon2,240,300)
r62_b,r62_lon = tbl.in_lon_range(b2,lon2,300,359) # remember that 360 is recorded as 0
# brightness 3 over 6 longitude ranges
r13_b,r13_lon = tbl.in_lon_range(b3,lon3,0,60)
r23_b,r23_lon = tbl.in_lon_range(b3,lon3,60,120)
r33_b,r33_lon = tbl.in_lon_range(b3,lon3,120,180)
r43_b,r43_lon = tbl.in_lon_range(b3,lon3,180,240)
r53_b,r53_lon = tbl.in_lon_range(b3,lon3,240,300)
r63_b,r63_lon = tbl.in_lon_range(b3,lon3,300,359)
# counting up totals to make bar graph
r11_tot = np.shape(r11_b)[0]
r12_tot = np.shape(r12_b)[0]
r13_tot = np.shape(r13_b)[0]
r21_tot = np.shape(r21_b)[0]
r22_tot = np.shape(r22_b)[0]
r23_tot = np.shape(r23_b)[0]
r31_tot = np.shape(r31_b)[0]
r32_tot = np.shape(r32_b)[0]
r33_tot = np.shape(r33_b)[0]
r41_tot = np.shape(r41_b)[0]
r42_tot = np.shape(r42_b)[0]
r43_tot = np.shape(r43_b)[0]
r51_tot = np.shape(r51_b)[0]
r52_tot = np.shape(r52_b)[0]
r53_tot = np.shape(r53_b)[0]
r61_tot = np.shape(r61_b)[0]
r62_tot = np.shape(r62_b)[0]
r63_tot = np.shape(r63_b)[0]
# totals formated as bar graph data
tot_b1 = (r11_tot,r21_tot,r31_tot,r41_tot,r51_tot,r61_tot)
tot_b2 = (r12_tot,r22_tot,r32_tot,r42_tot,r52_tot,r62_tot)
tot_b3 = (r13_tot,r23_tot,r33_tot,r43_tot,r53_tot,r63_tot)
# create plot
fig, ax = plt.subplots()
index = np.arange(n_ranges)
bar_width = 0.25
opacity = 0.8
# create a bar set for brightness 1
rects1 = plt.bar(index, tot_b1, bar_width,
alpha=opacity,
color='r',
label='(1) Faint')
# create bar set for brightness 2
rects2 = plt.bar(index+bar_width, tot_b2, bar_width,
alpha=opacity,
color='b',
label='(2) Intermediate')
# create bar set for brightness 3
rects3 = plt.bar(index + (2*bar_width), tot_b3, bar_width,
alpha=opacity,
color='g',
label='(3) Bright')

plt.xlabel('Longitudes',fontsize=12)
plt.ylabel('# of Cloud Bands (of {} detections)'.format(tot_detect),fontsize=12)
plt.title('Brightness of Cloud Band by Longitude Range',fontsize=15)
plt.xticks(index + bar_width, ('0-60', '60-120', '120-180', '180-240','240-300','300-360'))
plt.legend(loc='best',fancybox=True,framealpha=0.25)

plt.tight_layout()
plt.show()

# ex) 2 -- bar of cloud smoothness for different longitude ranges
# ---------------------------------------------------------------------

n_ranges = 6
o1,lon1,s1,b1,g_c1 = tbl.from_val('7200-7600.fits','smooth',1)
o2,lon2,s2,b2,g_c2 = tbl.from_val('7200-7600.fits','smooth',2)
o3,lon3,s3,b3,g_c3 = tbl.from_val('7200-7600.fits','smooth',3)
#brightness 1 over 6 longitude ranges
r11_s,r11_lon = tbl.in_lon_range(s1,lon1,0,60)
r21_s,r21_lon = tbl.in_lon_range(s1,lon1,60,120)
r31_s,r31_lon = tbl.in_lon_range(s1,lon1,120,180)
r41_s,r41_lon = tbl.in_lon_range(s1,lon1,180,240)
r51_s,r51_lon = tbl.in_lon_range(s1,lon1,240,300)
r61_s,r61_lon = tbl.in_lon_range(s1,lon1,300,359) # remember that 360 is recorded as 0
# brightness 2 over 6 longitude ranges
# notation: rxy is range x (of 4), brightness y (of 3) ; b is brightness array, lon is longitude range corresponding to it
r12_s,r12_lon = tbl.in_lon_range(s2,lon2,0,60)
r22_s,r22_lon = tbl.in_lon_range(s2,lon2,60,120)
r32_s,r32_lon = tbl.in_lon_range(s2,lon2,120,180)
r42_s,r42_lon = tbl.in_lon_range(s2,lon2,180,240)
r52_s,r52_lon = tbl.in_lon_range(s2,lon2,240,300)
r62_s,r62_lon = tbl.in_lon_range(s2,lon2,300,359) # remember that 360 is recorded as 0
# brightness 3 over 4 longitude ranges
r13_s,r13_lon = tbl.in_lon_range(s3,lon3,0,60)
r23_s,r23_lon = tbl.in_lon_range(s3,lon3,60,120)
r33_s,r33_lon = tbl.in_lon_range(s3,lon3,120,180)
r43_s,r43_lon = tbl.in_lon_range(s3,lon3,180,240)
r53_s,r53_lon = tbl.in_lon_range(s3,lon3,240,300)
r63_s,r63_lon = tbl.in_lon_range(s3,lon3,300,359)
# counting up totals to make bar graph
r11_tot = np.shape(r11_s)[0]
r12_tot = np.shape(r12_s)[0]
r13_tot = np.shape(r13_s)[0]
r21_tot = np.shape(r21_s)[0]
r22_tot = np.shape(r22_s)[0]
r23_tot = np.shape(r23_s)[0]
r31_tot = np.shape(r31_s)[0]
r32_tot = np.shape(r32_s)[0]
r33_tot = np.shape(r33_s)[0]
r41_tot = np.shape(r41_s)[0]
r42_tot = np.shape(r42_s)[0]
r43_tot = np.shape(r43_s)[0]
r51_tot = np.shape(r51_s)[0]
r52_tot = np.shape(r52_s)[0]
r53_tot = np.shape(r53_s)[0]
r61_tot = np.shape(r61_s)[0]
r62_tot = np.shape(r62_s)[0]
r63_tot = np.shape(r63_s)[0]

tot_detect = np.shape(g_c1)[0]+np.shape(g_c2)[0]+np.shape(g_c3)[0]

# totals formated as bar graph data
tot_s1 = (r11_tot,r21_tot,r31_tot,r41_tot,r51_tot,r61_tot)
tot_s2 = (r12_tot,r22_tot,r32_tot,r42_tot,r52_tot,r62_tot)
tot_s3 = (r13_tot,r23_tot,r33_tot,r43_tot,r53_tot,r63_tot)
# create plot
fig, ax = plt.subplots()
index = np.arange(n_ranges)
bar_width = 0.25
opacity = 0.8
# create bar set for brightness 2
rects1 = plt.bar(index, tot_s1, bar_width,
alpha=opacity,
color='b',
label='(1) Smooth')
# create bar set for brightness 3
rects2 = plt.bar(index + bar_width, tot_s2, bar_width,
alpha=opacity,
color='g',
label='(2) Intermediate')

rects3 = plt.bar(index + (2*bar_width), tot_s3, bar_width,
alpha=opacity,
color='r',
label='(3) Rough/Fragmented')

plt.xlabel('Longitudes', fontsize=12)
plt.ylabel('# of Cloud Bands (of {} detections)'.format(tot_detect), fontsize=12)
plt.title('Smoothness of Cloud Band by Longitude Range', fontsize=15)
plt.xticks(index + bar_width, ('0-60', '60-120', '120-180', '180-240','240-300','300-360'))
plt.legend(loc='best',fancybox=True,framealpha=0.25)

plt.tight_layout()
plt.show()
        
# ex4) # of detections vs longitude
# histogram

o, lon, s, b, g_c, d = tbl.trim_data_cb('7200-7600.fits')

x = np.array([])
y = np.array([])
    
for j in np.arange(360):
    for i in np.arange(np.shape(d)[0]):
        if j in lon[i]:
            x = np.append(x,j)
            

total = np.shape(d)[0]

fig,ax = plt.subplots(figsize=(16,7))
ax.xaxis.set_minor_locator(ticker.MultipleLocator(10))
ax.yaxis.set_minor_locator(ticker.MultipleLocator(1))
n,bins,patches = plt.hist(x,360)

the_mean = np.mean(n)
ax.axhline(the_mean, color='r', linestyle='--',label='Mean detections across degree bins')
plt.legend(loc='best',fancybox=True,framealpha=0.3)

plt.xlabel('Longitude (degrees East)',fontsize=15)
plt.ylabel('# of Detections ({} total)'.format(total),fontsize=15)
plt.title('Detections vs Longitude',fontsize=20)
plt.ylim(11,28)
plt.show()
        
        
        
        
        
