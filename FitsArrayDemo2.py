# -*- coding: utf-8 -*-
"""
Created on Mon Jul 29 17:44:01 2019

@author: silve
"""

from astropy.io import fits
import numpy as np
import ql_mod as tbl
import matplotlib.pyplot as plt
import math
import matplotlib.ticker as ticker


# making some more demo graphs

# ex5) # of detections in each brightness value       
o1,lon1,s1,b1,g_c1 = tbl.from_val('7200-7600.fits','bright',1)
o2,lon2,s2,b2,g_c2 = tbl.from_val('7200-7600.fits','bright',2)
o3,lon3,s3,b3,g_c3 = tbl.from_val('7200-7600.fits','bright',3)

x1 = np.array([])
y1 = np.array([])
    
for j in np.arange(360):
    for i in np.arange(np.shape(o1)[0]):
        if j in lon1[i]:
            x1 = np.append(x1,j)

x2 = np.array([])
y2 = np.array([])
    
for j in np.arange(360):
    for i in np.arange(np.shape(o2)[0]):
        if j in lon2[i]:
            x2 = np.append(x2,j)

x3 = np.array([])
y3 = np.array([])
    
for j in np.arange(360):
    for i in np.arange(np.shape(o3)[0]):
        if j in lon3[i]:
            x3 = np.append(x3,j)
# first plot for bright = 1         
total1 = np.shape(o1)[0]

fig,ax = plt.subplots(figsize=(16,7))
ax.xaxis.set_minor_locator(ticker.MultipleLocator(10))
ax.yaxis.set_minor_locator(ticker.MultipleLocator(1))
n,bins,patches = plt.hist(x1,360,color='firebrick')

the_mean = np.mean(n)
ax.axhline(the_mean, color='r', linestyle='--',label='Mean detections across degree bins')
plt.legend(loc='best',fancybox=True,framealpha=0.3)

plt.xlabel('Longitude (degrees East)',fontsize=15)
plt.ylabel('# of Detections ({} total)'.format(total1),fontsize=15)
plt.title('"Faint" Detections (brightness=1)',fontsize=20)
plt.ylim(0,11)
plt.show()

# second plot for bright = 2
total2 = np.shape(o2)[0]

fig,ax = plt.subplots(figsize=(16,7))
ax.xaxis.set_minor_locator(ticker.MultipleLocator(10))
ax.yaxis.set_minor_locator(ticker.MultipleLocator(1))
n,bins,patches = plt.hist(x2,360,color='darkcyan')

the_mean = np.mean(n)
ax.axhline(the_mean, color='r', linestyle='--',label='Mean detections across degree bins')
plt.legend(loc='best',fancybox=True,framealpha=0.3)

plt.xlabel('Longitude (degrees East)',fontsize=15)
plt.ylabel('# of Detections ({} total)'.format(total2),fontsize=15)
plt.title('"Intermediate" Detections (brightness=2)',fontsize=20)
plt.ylim(4,16)
plt.show()

# third plot for bright = 3
total3 = np.shape(o3)[0]

fig,ax = plt.subplots(figsize=(16,7))
ax.xaxis.set_minor_locator(ticker.MultipleLocator(10))
ax.yaxis.set_minor_locator(ticker.MultipleLocator(1))
n,bins,patches = plt.hist(x3,360,color='olivedrab')

the_mean = np.mean(n)
ax.axhline(the_mean, color='r', linestyle='--',label='Mean detections across degree bins')
plt.legend(loc='best',fancybox=True,framealpha=0.3)

plt.xlabel('Longitude (degrees East)',fontsize=15)
plt.ylabel('# of Detections ({} total)'.format(total3),fontsize=15)
plt.title('"Bright" Detections (brightness=3)',fontsize=20)
plt.ylim(0,11)
plt.show()        
        
        
# ex6) # of detections in each smoothness value       
o1,lon1,s1,b1,g_c1 = tbl.from_val('7200-7600.fits','smooth',1)
o2,lon2,s2,b2,g_c2 = tbl.from_val('7200-7600.fits','smooth',2)
o3,lon3,s3,b3,g_c3 = tbl.from_val('7200-7600.fits','smooth',3)

x1 = np.array([])
y1 = np.array([])
    
for j in np.arange(360):
    for i in np.arange(np.shape(o1)[0]):
        if j in lon1[i]:
            x1 = np.append(x1,j)

x2 = np.array([])
y2 = np.array([])
    
for j in np.arange(360):
    for i in np.arange(np.shape(o2)[0]):
        if j in lon2[i]:
            x2 = np.append(x2,j)

x3 = np.array([])
y3 = np.array([])
    
for j in np.arange(360):
    for i in np.arange(np.shape(o3)[0]):
        if j in lon3[i]:
            x3 = np.append(x3,j)
# first plot for smooth = 1         
total1 = np.shape(o1)[0]

fig,ax = plt.subplots(figsize=(16,7))
ax.xaxis.set_minor_locator(ticker.MultipleLocator(10))
ax.yaxis.set_minor_locator(ticker.MultipleLocator(1))
n,bins,patches = plt.hist(x1,360,color='firebrick')

the_mean = np.mean(n)
ax.axhline(the_mean, color='r', linestyle='--',label='Mean detections across degree bins')
plt.legend(loc='best',fancybox=True,framealpha=0.3)

plt.xlabel('Longitude (degrees East)',fontsize=15)
plt.ylabel('# of Detections ({} total)'.format(total1),fontsize=15)
plt.title('"Smooth" Detections (smoothness=1)',fontsize=20)
plt.ylim(0,10)
plt.show()

# second plot for bright = 2
total2 = np.shape(o2)[0]

fig,ax = plt.subplots(figsize=(16,7))
ax.xaxis.set_minor_locator(ticker.MultipleLocator(10))
ax.yaxis.set_minor_locator(ticker.MultipleLocator(1))
n,bins,patches = plt.hist(x2,360,color='darkcyan')

the_mean = np.mean(n)
ax.axhline(the_mean, color='r', linestyle='--',label='Mean detections across degree bins')
plt.legend(loc='best',fancybox=True,framealpha=0.3)

plt.xlabel('Longitude (degrees East)',fontsize=15)
plt.ylabel('# of Detections ({} total)'.format(total2),fontsize=15)
plt.title('"Intermediate" Detections (smoothness=2)',fontsize=20)
plt.ylim(0,16)
plt.show()

# third plot for bright = 3
total3 = np.shape(o3)[0]

fig,ax = plt.subplots(figsize=(16,7))
ax.xaxis.set_minor_locator(ticker.MultipleLocator(10))
ax.yaxis.set_minor_locator(ticker.MultipleLocator(1))
n,bins,patches = plt.hist(x3,360,color='olivedrab')

the_mean = np.mean(n)
ax.axhline(the_mean, color='r', linestyle='--',label='Mean detections across degree bins')
plt.legend(loc='best',fancybox=True,framealpha=0.3)

plt.xlabel('Longitude (degrees East)',fontsize=15)
plt.ylabel('# of Detections ({} total)'.format(total3),fontsize=15)
plt.title('"Rough/Fragmented" Detections (smoothness=3)',fontsize=20)
plt.ylim(0,17)
plt.show()        