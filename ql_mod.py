# -*- coding: utf-8 -*-
"""
Created on Sun Aug 11 18:01:39 2019

@author: silve
"""

import numpy as np
from astropy.io import fits
#from shutil import copy2 --> for later if I want copying functionality

def mkfits(first, second, filename, qual=133): # creates fits table with special column format for IUVS quicklooks
    # default quality is 133
    # building columns for orbit number given

    orbitz = np.arange(first, second)
    qual_tbl = np.zeros(len(orbitz))
    for i in np.arange(len(orbitz)):
        qual_tbl[i] = qual
        

    # general information
    col1 = fits.Column(name='orbit', format='I', array=orbitz)
    col2 = fits.Column(name='quality', format='I', array=qual_tbl)
    col3 = fits.Column(name='illum', format='D')
    col4 = fits.Column(name='clarity', format='I')
    col5 = fits.Column(name='pole_n', format='L')
    col6 = fits.Column(name='pole_s', format='L')
    col7 = fits.Column(name='pole_vis', format='L')

    # features
    col8 = fits.Column(name='cloud_gen', format='28I',dim='(7,4)')
    col9 = fits.Column(name='cloud_top', format='16I',dim='(4,4)')
    col10 = fits.Column(name='dust', format='28I',dim='(7,4)')
    col11 = fits.Column(name='grav_wav', format='28I',dim='(7,4)')
    col12 = fits.Column(name='ozone_n', format='L')
    col13 = fits.Column(name='ozone_s', format='L')
    col14 = fits.Column(name='other', format='A200')

    coldefs = fits.ColDefs([col1, col2, col3, col4, col5, col6, col7,
                        col8, col9, col10, col11, col12, col13, col14])
    hdu = fits.BinTableHDU.from_columns(coldefs)
    hdu.writeto(filename, overwrite=True)
    
    print '\n\nFile created(or overwritten):', filename,'\n\n'

# gets row (index) for a given orbit
    
def mkfits2(first, second, filename):
    orbitz = np.arange(first, second)
    col1 = fits.Column(name='orbit', format='I', array=orbitz)
    col2 = fits.Column(name='band_vis', format='L')
    col3 = fits.Column(name='smooth', format='I')
    col4 = fits.Column(name='bright', format='I')
    col5 = fits.Column(name='longitude', format='2I', dim='2')
    col6 = fits.Column(name='gap_cloud', format='I')
    col7 = fits.Column(name='other', format='A200')
    
    coldefs = fits.ColDefs([col1, col2, col3, col4, col5, col6, col7])
    hdu = fits.BinTableHDU.from_columns(coldefs)
    hdu.writeto(filename, overwrite=True)
    
    print '\n\nFile created (or overwritten):', filename,'\n\n'
    
def get_row(filename, orbit):
    hdul = fits.open(filename, mode='readonly', memmap=False)  # open a FITS file with update mode
    data = hdul[1].data
    row = np.where(data['orbit'] == orbit)[0][0]
    hdul.close()
    return row

    
# updates data for a given .fits table and subfield
# the most technical function syntax-wise, building block for all other functions
def change(filename, orbit, colname, value, ind1=None, ind2=None):
    
    hdul = fits.open(filename, mode='update', memmap=False)  # open a FITS file with update mode
    data = hdul[1].data
    row = np.where(data['orbit'] == orbit)[0][0]
    # makes sure indexing syntax is appropriate for the column to be changed
    if ind1 == None and ind2 == None: 
        hdul[1].data[row][colname] = value
        print '\nUpdated value is:',hdul[1].data[row][colname]
    elif ind1 != None and ind2 == None:
        hdul[1].data[row][colname][ind1] = value
        print '\nUpdated value is:',hdul[1].data[row][colname][ind1]
    elif ind1 != None and ind2 != None:
        hdul[1].data[row][colname][ind1][ind2] = value
        print '\nUpdated value is:',hdul[1].data[row][colname][ind1][ind2]
    else:
        print 'Something went wrong with the indexes.'

    hdul.close() #this automatically updates original file while in 'update' mode
    
def general(filename, orbit): # general information
    illum = input('Enter the illumination percentage (as a decimal):')
    change(filename, orbit, 'illum', illum)
    
    clarity = input('Enter the image clarity (1 thru 5):')
    change(filename, orbit, 'clarity', clarity)
    
    pole_n = input('Is the North pole oriented in the image? (True[1] or False[0]):')
    change(filename, orbit, 'pole_n', pole_n)
    if pole_n == True:
        change(filename, orbit, 'pole_s', False)
    else:
        change(filename, orbit, 'pole_s', True)
    
    pole_vis = input('Is the pole visible? (True[1] or False[0]):')
    change(filename, orbit, 'pole_vis', pole_vis)
    
    print '\nEnd of general information'
    
def clouds_gen(filename, orbit): #clouds (general)
    cloud1 = input('Are there general clouds in the image? (True[1] or False[0]):')
    cloud_main = True
    if cloud1 == True and cloud_main == True:
        cld_g00 = 1
        change(filename, orbit, 'cloud_gen', cld_g00, ind1=0, ind2=0)
        cld_g01 = input('Enter Lower edge (LT):')
        change(filename, orbit, 'cloud_gen', cld_g01, ind1=0, ind2=1)
        cld_g02 = input('Enter Upper edge (LT):')
        change(filename, orbit, 'cloud_gen', cld_g02, ind1=0, ind2=2)
        cld_g03 = input('Enter Lower edge (latitude: South is - North is +):')
        change(filename, orbit, 'cloud_gen', cld_g03, ind1=0, ind2=3)
        cld_g04 = input('Enter Upper edge (latitude: South is - North is +):')
        change(filename, orbit, 'cloud_gen', cld_g04, ind1=0, ind2=4)
        cld_g05 = input('Enter Western edge (longitude):')
        change(filename, orbit, 'cloud_gen', cld_g05, ind1=0, ind2=5)
        cld_g06 = input('Enter Eastern edge (longitude):')
        change(filename, orbit, 'cloud_gen', cld_g06, ind1=0, ind2=6)
    elif cloud1 == False:
        cloud_main = False
        
    if cloud_main == True:
        cloud2 = input('Are there any more general clouds in the image? (True[1] or False[0]):')
        if cloud2 == True:
            cld_g10 = 1
            change(filename, orbit, 'cloud_gen', cld_g10, ind1=1, ind2=0)
            cld_g11 = input('Enter Lower edge (LT):')
            change(filename, orbit, 'cloud_gen', cld_g11, ind1=1, ind2=1)
            cld_g12 = input('Enter Upper edge (LT):')
            change(filename, orbit, 'cloud_gen', cld_g12, ind1=1, ind2=2)
            cld_g13 = input('Enter Lower edge (latitude: South is - North is +):')
            change(filename, orbit, 'cloud_gen', cld_g13, ind1=1, ind2=3)
            cld_g14 = input('Enter Upper edge (latitude: South is - North is +):')
            change(filename, orbit, 'cloud_gen', cld_g14, ind1=1, ind2=4)
            cld_g15 = input('Enter Western edge (longitude):')
            change(filename, orbit, 'cloud_gen', cld_g15, ind1=1, ind2=5)
            cld_g16 = input('Enter Eastern edge (longitude):')
            change(filename, orbit, 'cloud_gen', cld_g16, ind1=1, ind2=6)
        elif cloud2 == False:
            cloud_main = False
        
    if cloud_main == True:
        cloud3 = input('Are there any more general clouds in the image? (True[1] or False[0]):')
        if cloud3 == True:
            cld_g20 = 1
            change(filename, orbit, 'cloud_gen', cld_g20, ind1=2, ind2=0)
            cld_g21 = input('Enter Lower edge (LT):')
            change(filename, orbit, 'cloud_gen', cld_g21, ind1=2, ind2=1)
            cld_g22 = input('Enter Upper edge (LT):')
            change(filename, orbit, 'cloud_gen', cld_g22, ind1=2, ind2=2)
            cld_g23 = input('Enter Lower edge (latitude: South is - North is +):')
            change(filename, orbit, 'cloud_gen', cld_g23, ind1=2, ind2=3)
            cld_g24 = input('Enter Upper edge (latitude: South is - North is +):')
            change(filename, orbit, 'cloud_gen', cld_g24, ind1=2, ind2=4)
            cld_g25 = input('Enter Western edge (longitude):')
            change(filename, orbit, 'cloud_gen', cld_g25, ind1=2, ind2=5)
            cld_g26 = input('Enter Eastern edge (longitude):')
            change(filename, orbit, 'cloud_gen', cld_g26, ind1=2, ind2=6)
        elif cloud3 == False:
            cloud_main = False
        
    if cloud_main == True:
        cloud4 = input('Are there any more general clouds in the image? (True[1] or False[0]):')
        if cloud4 == True:
            cld_g30 = 1
            change(filename, orbit, 'cloud_gen', cld_g30, ind1=3, ind2=0)
            cld_g31 = input('Enter Lower edge (LT):')
            change(filename, orbit, 'cloud_gen', cld_g31, ind1=3, ind2=1)
            cld_g32 = input('Enter Upper edge (LT):')
            change(filename, orbit, 'cloud_gen', cld_g32, ind1=3, ind2=2)
            cld_g33 = input('Enter Lower edge (latitude: South is - North is +):')
            change(filename, orbit, 'cloud_gen', cld_g33, ind1=3, ind2=3)
            cld_g34 = input('Enter Upper edge (latitude: South is - North is +):')
            change(filename, orbit, 'cloud_gen', cld_g34, ind1=3, ind2=4)
            cld_g35 = input('Enter Western edge (longitude):')
            change(filename, orbit, 'cloud_gen', cld_g35, ind1=3, ind2=5)
            cld_g36 = input('Enter Eastern edge (longitude):')
            change(filename, orbit, 'cloud_gen', cld_g36, ind1=3, ind2=6)
        else:
            print '\nEnd of clouds (general)'
        
def clouds_top(filename, orbit): # clouds (topographic)
    topo1 = input('Are there topographic clouds in the image? (True[1] or False[0]):')
    topo_main = True
    if topo1 == True and topo_main == True:
        cld_t00 = 1
        change(filename, orbit, 'cloud_top', cld_t00, ind1=0, ind2=0)
        cld_t01 = input('Enter Lower edge (LT):')
        change(filename, orbit, 'cloud_top', cld_t01, ind1=0, ind2=1)
        cld_t02 = input('Enter Upper edge (LT):')
        change(filename, orbit, 'cloud_top', cld_t02, ind1=0, ind2=2)
        cld_t03 = input('Enter number corresponding to the volcano (see documentation):')
        change(filename, orbit, 'cloud_top', cld_t03, ind1=0, ind2=3)
    elif topo1 == False:
        topo_main = False

    if topo_main == True:    
        topo2 = input('Are there any more topographic clouds in the image? (True[1] or False[0]):')
        if topo2 == True:
            cld_t10 = 1
            change(filename, orbit, 'cloud_top', cld_t10, ind1=1, ind2=0)
            cld_t11 = input('Enter Lower edge (LT):')
            change(filename, orbit, 'cloud_top', cld_t11, ind1=1, ind2=1)
            cld_t12 = input('Enter Upper edge (LT):')
            change(filename, orbit, 'cloud_top', cld_t12, ind1=1, ind2=2)
            cld_t13 = input('Enter number corresponding to the volcano (see documentation):')
            change(filename, orbit, 'cloud_top', cld_t13, ind1=1, ind2=3)
        elif topo2 == False:
            topo_main = False    
    
    if topo_main == True:  
        topo3 = input('Are there any more topographic clouds in the image? (True[1] or False[0]):')
        if topo3 == True:
            cld_t20 = 1
            change(filename, orbit, 'cloud_top', cld_t20, ind1=2, ind2=0)
            cld_t21 = input('Enter Lower edge (LT):')
            change(filename, orbit, 'cloud_top', cld_t21, ind1=2, ind2=1)
            cld_t22 = input('Enter Upper edge (LT):')
            change(filename, orbit, 'cloud_top', cld_t22, ind1=2, ind2=2)
            cld_t23 = input('Enter number corresponding to the volcano (see documentation):')
            change(filename, orbit, 'cloud_top', cld_t23, ind1=2, ind2=3)
        elif topo3 == False:
            topo_main = False    
        
    if topo_main == True: 
        topo4 = input('Are there any more topographic clouds in the image? (True[1] or False[0]):')
        if topo4 == True:
            cld_t30 = 1
            change(filename, orbit, 'cloud_top', cld_t30, ind1=3, ind2=0)
            cld_t31 = input('Enter Lower edge (LT):')
            change(filename, orbit, 'cloud_top', cld_t31, ind1=3, ind2=1)
            cld_t32 = input('Enter Upper edge (LT):')
            change(filename, orbit, 'cloud_top', cld_t32, ind1=3, ind2=2)
            cld_t33 = input('Enter number corresponding to the volcano (see documentation):')
            change(filename, orbit, 'cloud_top', cld_t33, ind1=3, ind2=3)
            
    print '\nEnd of clouds (topographic)'

def dust(filename, orbit):# dust
    dust1 = input('Is there dust in the image? (True[1] or False[0]):')
    dust_main = True
    if dust1 == True and dust_main == True:
        dst_00 = 1
        change(filename, orbit, 'dust', dst_00, ind1=0, ind2=0)
        dst_01 = input('Enter Lower edge of dust (LT):')
        change(filename, orbit, 'dust', dst_01, ind1=0, ind2=1)
        dst_02 = input('Enter Upper edge of dust (LT):')
        change(filename, orbit, 'dust', dst_02, ind1=0, ind2=2)
        dst_03 = input('Enter Lower edge of dust (latitude: South is - North is +):')
        change(filename, orbit, 'dust', dst_03, ind1=0, ind2=3)
        dst_04 = input('Enter Upper edge of dust (latitude: South is - North is +):')
        change(filename, orbit, 'dust', dst_04, ind1=0, ind2=4)
        dst_05 = input('Enter Western edge of dust (longitude):')
        change(filename, orbit, 'dust', dst_05, ind1=0, ind2=5)
        dst_06 = input('Enter Eastern edge of dust (longitude):')
        change(filename, orbit, 'dust', dst_06, ind1=0, ind2=6)
    elif dust1 == False:
        dust_main = False
        
    if dust_main == True:
        dust2 = input('Is there any more dust in the image? (True[1] or False[0]):')
        if dust2 == True:
            dst_10 = 1
            change(filename, orbit, 'dust', dst_10, ind1=1, ind2=0)
            dst_11 = input('Enter Lower edge of dust (LT):')
            change(filename, orbit, 'dust', dst_11, ind1=1, ind2=1)
            dst_12 = input('Enter Upper edge of dust (LT):')
            change(filename, orbit, 'dust', dst_12, ind1=1, ind2=2)
            dst_13 = input('Enter Lower edge of dust (latitude: South is - North is +):')
            change(filename, orbit, 'dust', dst_13, ind1=1, ind2=3)
            dst_14 = input('Enter Upper edge of dust (latitude: South is - North is +):')
            change(filename, orbit, 'dust', dst_14, ind1=1, ind2=4)
            dst_15 = input('Enter Western edge of dust (longitude):')
            change(filename, orbit, 'dust', dst_15, ind1=1, ind2=5)
            dst_16 = input('Enter Eastern edge of dust (longitude):')
            change(filename, orbit, 'dust', dst_16, ind1=1, ind2=6)
        elif dust2 == False:
            dust_main = False
        
    if dust_main == True:
        dust3 = input('Is there any more dust in the image? (True[1] or False[0]):')
        if dust3 == True:
            dst_20 = 1
            change(filename, orbit, 'dust', dst_20, ind1=2, ind2=0)
            dst_21 = input('Enter Lower edge of dust (LT):')
            change(filename, orbit, 'dust', dst_21, ind1=2, ind2=1)
            dst_22 = input('Enter Upper edge of dust (LT):')
            change(filename, orbit, 'dust', dst_22, ind1=2, ind2=2)
            dst_23 = input('Enter Lower edge of dust (latitude: South is - North is +):')
            change(filename, orbit, 'dust', dst_23, ind1=2, ind2=3)
            dst_24 = input('Enter Upper edge of dust (latitude: South is - North is +):')
            change(filename, orbit, 'dust', dst_24, ind1=2, ind2=4)
            dst_25 = input('Enter Western edge of dust (longitude):')
            change(filename, orbit, 'dust', dst_25, ind1=2, ind2=5)
            dst_26 = input('Enter Eastern edge of dust (longitude):')
            change(filename, orbit, 'dust', dst_26, ind1=2, ind2=6)
        elif dust3 == False:
            dust_main = False
        
    if dust_main == True:
        dust4 = input('Is there any more dust in the image? (True[1] or False[0]):')
        if dust4 == True:
            dst_30 = 1
            change(filename, orbit, 'dust', dst_30, ind1=3, ind2=0)
            dst_31 = input('Enter Lower edge of dust (LT):')
            change(filename, orbit, 'dust', dst_31, ind1=3, ind2=1)
            dst_32 = input('Enter Upper edge of dust (LT):')
            change(filename, orbit, 'dust', dst_32, ind1=3, ind2=2)
            dst_33 = input('Enter Lower edge of dust (latitude: South is - North is +):')
            change(filename, orbit, 'dust', dst_33, ind1=3, ind2=3)
            dst_34 = input('Enter Upper edge of dust (latitude: South is - North is +):')
            change(filename, orbit, 'dust', dst_34, ind1=3, ind2=4)
            dst_35 = input('Enter Western edge of dust (longitude):')
            change(filename, orbit, 'dust', dst_35, ind1=3, ind2=5)
            dst_36 = input('Enter Eastern edge of dust (longitude):')
            change(filename, orbit, 'dust', dst_36, ind1=3, ind2=6)

    print '\nEnd of dust'  

def grav_wav(filename, orbit):
    grav1 = input('Are there gravity waves in the image? (True[1] or False[0]):')
    grav_main = True
    if grav1 == True and grav_main == True:
        grv_00 = 1
        change(filename, orbit, 'grav_wav', grv_00, ind1=0, ind2=0)
        grv_01 = input('Enter Lower edge of gravity waves (LT):')
        change(filename, orbit, 'grav_wav', grv_01, ind1=0, ind2=1)
        grv_02 = input('Enter Upper edge of gravity waves (LT):')
        change(filename, orbit, 'grav_wav', grv_02, ind1=0, ind2=2)
        grv_03 = input('Enter Lower edge of gravity waves (latitude: South is - North is +):')
        change(filename, orbit, 'grav_wav', grv_03, ind1=0, ind2=3)
        grv_04 = input('Enter Upper edge of gravity waves (latitude: South is - North is +):')
        change(filename, orbit, 'grav_wav', grv_04, ind1=0, ind2=4)
        grv_05 = input('Enter Western edge of gravity waves (longitude):')
        change(filename, orbit, 'grav_wav', grv_05, ind1=0, ind2=5)
        grv_06 = input('Enter Eastern edge of gravity waves (longitude):')
        change(filename, orbit, 'grav_wav', grv_06, ind1=0, ind2=6)
    elif grav1 == False:
        grav_main = False
        
    
    if grav_main == True:
        grav2 = input('Are there any more gravity waves in the image? (True[1] or False[0]):')
        if grav2 == True:
            grv_10 = 1
            change(filename, orbit, 'grav_wav', grv_10, ind1=1, ind2=0)
            grv_11 = input('Enter Lower edge of gravity waves (LT):')
            change(filename, orbit, 'grav_wav', grv_11, ind1=1, ind2=1)
            grv_12 = input('Enter Upper edge of gravity waves (LT):')
            change(filename, orbit, 'grav_wav', grv_12, ind1=1, ind2=2)
            grv_13 = input('Enter Lower edge of gravity waves (latitude: South is - North is +):')
            change(filename, orbit, 'grav_wav', grv_13, ind1=1, ind2=3)
            grv_14 = input('Enter Upper edge of gravity waves (latitude: South is - North is +):')
            change(filename, orbit, 'grav_wav', grv_14, ind1=1, ind2=4)
            grv_15 = input('Enter Western edge of gravity waves (longitude):')
            change(filename, orbit, 'grav_wav', grv_15, ind1=1, ind2=5)
            grv_16 = input('Enter Eastern edge of gravity waves (longitude):')
            change(filename, orbit, 'grav_wav', grv_16, ind1=1, ind2=6)
        elif grav2 == False:
            grav_main = False
        
    if grav_main == True:
        grav3 = input('Are there any more gravity waves in the image? (True[1] or False[0]):')
        if grav3 == True:
            grv_20 = 1
            change(filename, orbit, 'grav_wav', grv_20, ind1=2, ind2=0)
            grv_21 = input('Enter Lower edge of gravity waves (LT):')
            change(filename, orbit, 'grav_wav', grv_21, ind1=2, ind2=1)
            grv_22 = input('Enter Upper edge of gravity waves (LT):')
            change(filename, orbit, 'grav_wav', grv_22, ind1=2, ind2=2)
            grv_23 = input('Enter Lower edge of gravity waves (latitude: South is - North is +):')
            change(filename, orbit, 'grav_wav', grv_23, ind1=2, ind2=3)
            grv_24 = input('Enter Upper edge of gravity waves (latitude: South is - North is +):')
            change(filename, orbit, 'grav_wav', grv_24, ind1=2, ind2=4)
            grv_25 = input('Enter Western edge of gravity waves (longitude):')
            change(filename, orbit, 'grav_wav', grv_25, ind1=2, ind2=5)
            grv_26 = input('Enter Eastern edge of gravity waves (longitude):')
            change(filename, orbit, 'grav_wav', grv_26, ind1=2, ind2=6)
        elif grav3 == False:
            grav_main = False

    
    if grav_main == True:
        grav4 = input('Are there any more gravity waves in the image? (True[1] or False[0]):')
        if grav4 == True:
            grv_30 = 1
            change(filename, orbit, 'grav_wav', grv_30, ind1=3, ind2=0)
            grv_31 = input('Enter Lower edge of gravity waves (LT):')
            change(filename, orbit, 'grav_wav', grv_31, ind1=3, ind2=1)
            grv_32 = input('Enter Upper edge of gravity waves (LT):')
            change(filename, orbit, 'grav_wav', grv_32, ind1=3, ind2=2)
            grv_33 = input('Enter Lower edge of gravity waves (latitude: South is - North is +):')
            change(filename, orbit, 'grav_wav', grv_33, ind1=3, ind2=3)
            grv_34 = input('Enter Upper edge of gravity waves (latitude: South is - North is +):')
            change(filename, orbit, 'grav_wav', grv_34, ind1=3, ind2=4)
            grv_35 = input('Enter Western edge of gravity waves (longitude):')
            change(filename, orbit, 'grav_wav', grv_35, ind1=3, ind2=5)
            grv_36 = input('Enter Eastern edge of gravity waves (longitude):')
            change(filename, orbit, 'grav_wav', grv_36, ind1=3, ind2=6)

    print '\nEnd of gravity waves'  
    
def ozone(filename, orbit): # ozone north and south
    ozone_n = input('Is there ozone visible near the North pole? (True[1] or False[0]):')
    change(filename, orbit, 'ozone_n', ozone_n)
    ozone_s = input('Is there ozone visible near the South pole? (True[1] or False[0]):')
    change(filename, orbit, 'ozone_s', ozone_s)
    
def other(filename, orbit): # other (comment)
    other = raw_input('Type a comment for this orbit:')
    change(filename, orbit, 'other', other)

def entry(filename, orbit): # full entry for an orbit
    #general
    general(filename, orbit) 
    # features
    clouds_gen(filename, orbit) # clouds (general)
    clouds_top(filename, orbit) # clouds (topographic)
    dust(filename, orbit) # dust
    grav_wav(filename, orbit) # gravity waves
    ozone(filename, orbit) # ozone north and south
    other(filename, orbit) # other
    
    print '\n\nEntry complete for orbit #',orbit   


# full entry for cloudband table type
def entry2(filename, orbit):
    band_vis = input('Is the cloud band visible? (True[1] or False[0]):')
    change(filename, orbit, 'band_vis', band_vis)
    
    smooth = input('''
\n\nOptions for Smoothness:
     1 - smooth
     2 - intermediate
     3 - rough/fragmented
Enter the smoothness of the cloud band:''')
    change(filename, orbit, 'smooth', smooth)
    
    bright = input('''
\n\nOptions for Brightness:
     1 - faint
     2 - intermediate
     3 - bright
Enter the brightness of the cloud band:''')
    change(filename, orbit, 'bright', bright)
    
    lon1 = input('Enter Western edge (longitude):')
    change(filename, orbit, 'longitude', lon1, ind1=0)
    lon2 = input('Enter Eastern edge (longitude):')
    change(filename, orbit, 'longitude', lon2, ind1=1)
    
    gap_cloud = input('''
\n\nOptions for Clouds/Haze over Gap:
     1 - clear
     2 - sparse clouds
     3 - lots of clouds
     4 - haze
Enter the cloud/haze situation over the gap:''')
    change(filename, orbit, 'gap_cloud', gap_cloud)
    
    other(filename, orbit)
    
    print '\n\nEnd of cloud band entry for orbit #', orbit
    
    
    
def combine(filename1, filename2, newname):
    with fits.open(filename1) as hdul1:
        with fits.open(filename2) as hdul2:
            nrows1 = hdul1[1].data.shape[0]
            nrows2 = hdul2[1].data.shape[0]
            nrows = nrows1 + nrows2
            hdu = fits.BinTableHDU.from_columns(hdul1[1].columns, nrows=nrows)
            for colname in hdul1[1].columns.names:
                hdu.data[colname][nrows1:] = hdul2[1].data[colname] 
            hdu.writeto(newname, overwrite=True)
        
import matplotlib.pyplot as plt
import math
import matplotlib.ticker as ticker

# trim_data functions
# ---retrieves all valid data entries for specified column, removing empty ones

# trim_data()
# ---preserves continuous range of LT, lat, and lon as arrays stored in a list for each
# ---works for these columns: cloud_gen, dust, grav_wav
def trim_data1(filename, column):
    with fits.open(filename, memmap=False) as hdul:
        o = [] # orbit numbers
        t = [] # LT range of feature (array)
        lat = [] # latitude range of feature
        lon = [] # longitude range of feature
        data = hdul[1].data
        for index in np.arange(data['orbit'].shape[0]):
            for i in np.array([0,1,2,3]): # for columns w/ 4 entries
                condition = data[index][column][i][0]
                if condition == 1: # checks if not empty
					# LT range (t)
                    t_bound1 = data[index][column][i][1]
                    t_bound2 = data[index][column][i][2]
                    t_range = np.arange(t_bound1, t_bound2 + 1)
					# latitude range (lat)
                    lat_bound1 = data[index][column][i][3]
                    lat_bound2 = data[index][column][i][4]
                    lat_range = np.arange(lat_bound1, lat_bound2 + 1)
					# longitude range (lon)
                    lon_bound1 = data[index][column][i][5]
                    lon_bound2 = data[index][column][i][6]
                    if lon_bound1 > lon_bound2:
                        lon_pt1 = np.arange(lon_bound1, 360)
                        lon_pt2 = np.arange(0,lon_bound2 + 1)
                        lon_range = np.append(lon_pt1, lon_pt2)
                    else:
                        lon_range = np.arange(lon_bound1, lon_bound2 + 1)
                    # orbit number from index being looped
                    orb_num = data[index]['orbit']
					# appends output tables
                    o.append(orb_num)
                    t.append(t_range)
                    lat.append(lat_range)
                    lon.append(lon_range)
    o = np.array(o) 
    t = np.array(t)
    lat = np.array(lat)
    lon = np.array(lon)
    return o,t,lat,lon

# trim_data2()
# ---preserves continuous range of LT along with number of feature
# ---works only for cloud_top column
def trim_data2(filename, column):
    with fits.open(filename, memmap=False) as hdul:
		o = [] # orbit numbers
		t = [] # LT range of feature (array)
		vol = [] # volcano --> number corresponding to feature, see table_mod documentation
		data = hdul[1].data
		for index in np.arange(data['orbit'].shape[0]):
			for i in np.array([0,1,2,3]): # for columns w/ 4 entries
				condition = data[index][column][i][0]
				if condition == 1: # checks if not empty
					# LT range (t)
					t_bound1 = data[index][column][i][1]
					t_bound2 = data[index][column][i][2]
					t_range = np.arange(t_bound1, t_bound2 + 1)
					# latitude range (lat)
					vol_add = data[index][column][i][3]
                    # orbit number from index being looped
					orb_num = data[index]['orbit']
					# appends output tables
					o.append(orb_num)
					t.append(t_range)
					vol.append(vol_add)
    o = np.array(o)
    t = np.array(t)
    vol = np.array(vol)				
    return o, t, vol

# trim_data3()
# ---works for single entry columns: 
        #quality,illum,clarity,pole_n,pole_s,pole_vis,ozone_n,ozone_s
def trim_data3(filename, column):
    with fits.open(filename, memmap=False) as hdul:
		o = [] # orbit numbers
		v = [] # value from column
		data = hdul[1].data
		for index in np.arange(data['orbit'].shape[0]):
			# LT range (t)
			v_add = data[index][column]
            # orbit number
			orb_num = data[index]['orbit']
			# appends output tables
			o.append(orb_num)
			v.append(v_add)
    o = np.array(o)
    v = np.array(v)				
    return o,v

# trim_data_cb()
# ---works for cloud band table columns: 
        #returns all non-empty entries with all columns
        #orbit,band_vis,smooth,bright,longitude,gap_cloud,other
        #longitude is converted into an array of the range covered by the band across the image
def trim_data_cb(filename, mode=1):
    # mode 1 returns two arrays, listing orbit number and column value
    if mode==1:
        with fits.open(filename, memmap=False) as hdul:
            d = []
            o = [] # orbit numbers
            s = [] # smoothness of cloud band
            b = [] # brightness of cloud band
            lon = [] # longitude range of feature
            g_c = []
            data = hdul[1].data
            for index in np.arange(data['orbit'].shape[0]):
                condition = data[index]['band_vis']
                if condition == 1: # checks if band is visible in each orbit
					# longitude range (lon)
                    lon_bound1 = data[index]['longitude'][0]
                    lon_bound2 = data[index]['longitude'][1]
                    if lon_bound1 > lon_bound2:
                        lon_pt1 = np.arange(lon_bound1, 360)
                        lon_pt2 = np.arange(0,lon_bound2 + 1)
                        lon_range = np.append(lon_pt1, lon_pt2)
                    else:
                        lon_range = np.arange(lon_bound1, lon_bound2 + 1)
                    # orbit number from index being looped
                    orbit = data[index]['orbit']
                    smooth = data[index]['smooth']
                    bright = data[index]['bright']
                    gap_cloud = data[index]['gap_cloud']
					# appends output tables
                    d.append(True)
                    o.append(orbit)
                    lon.append(lon_range)
                    s.append(smooth)
                    b.append(bright)
                    g_c.append(gap_cloud)
            o = np.array(o)
            lon = np.array(lon)
            s = np.array(s)
            b = np.array(b)
            g_c = np.array(g_c)
            d = np.array(d)
        return o, lon, s, b, g_c, d
            
            
# the mother of all trim_data functions
# ---evaluates from among 1,2, and 3 based on given column name
# ---us this one for convenience
def trim_data(filename, column):
    if column == ('cloud_gen' or 'dust' or 'grav_wav'):
        o,t,lat,lon = trim_data1(filename, column)
        return o,t,lat,lon
    elif column == ('cloud_top'):
        o,t,vol = trim_data2(filename, column)
        return o,t,vol
    elif column == ('quality' or 'illum' or 'clarity' or 'pole_n'
                    or 'pole_s' or 'pole_vis' or 'ozone_n' or 'ozone_s'):
        o,v = trim_data3(filename, column)
        return o,v

def from_val(filename,column,value): # populates data only if specified value is in the row
    # column specifies where to search for the value
    # not currently designed for longitude range -> do externally looking into lon array
    
    o, lon, s, b, g_c, d = trim_data_cb(filename)
    colnames = [['orbit',o,0],['smooth',s,1],['bright',b,2],['gap_cloud',g_c,3]]
    colarray = np.array([colnames[0][0],colnames[1][0],colnames[2][0],colnames[3][0]])
    
    for i in np.arange(4):
        if column == colarray[i]:
            t = np.array(colnames[i][1])
          
    mask = np.zeros(t.shape[0], dtype=bool) # empty array for boolean 'mask'
    for i in np.arange(t.shape[0]): #iterate over trimmed LT array, populate mask bool array
    		if value == t[i]:
	    		mask[i] = True
	    	else:
	    		mask[i] = False
    
    filtered_o = o[mask] # creating filtered data from mask
    filtered_lon = lon[mask]
    filtered_s = s[mask]
    filtered_b = b[mask]
    filtered_g_c = g_c[mask]
    
    return filtered_o,filtered_lon,filtered_s,filtered_b,filtered_g_c
            
def in_lon_range(array_values,array_lon,cond1,cond2):
    # populates data only if within specified longitude range
    # usually these are already taken from a trim_data function
    # not calibrated for crossing 0 degrees right now ie 355-5, cond1 must be < than cond2
    v = array_values
    x = array_lon
    
    mask = np.zeros(v.shape[0], dtype=bool) # empty array for boolean 'mask'

    for i in np.arange(x.shape[0]): #iterate over trimmed longitude array, populate mask bool array
        for j in np.arange(cond1, cond2 + 1):
            if j in x[i]:
                mask[i] = True
                break
            else:
                mask[i] = False
            
    v2 = v[mask] # creating filtered data from mask over values
    x2 = x[mask] # full longitude range for selected rows of v
    return v2,x2

# Demo / Test ---------------------------------------------------------

#mkfits(3000,3100,'3000-3100.fits')
#change('1000-1100.fits',1001,'cloud_gen',12,ind1=1,ind2=0)
#entry('3000-3100.fits',3001)
#ozone('3000-3100.fits', 3002)